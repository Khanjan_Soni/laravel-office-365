<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

// Route for Login
Route::get('/signin', 'AuthController@signin');
Route::get('/callback', 'AuthController@callback');

// Route for fetching caleder events
Route::get('/calendar', 'CalendarController@calendar');

// Route for logout
Route::get('/signout', 'AuthController@signout');


