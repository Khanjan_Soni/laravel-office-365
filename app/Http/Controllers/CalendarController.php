<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\TimeZones\TimeZones;
use App\TokenStore\TokenCache;
use Carbon\Carbon;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class CalendarController extends Controller
{
    public function calendar()
    {
        $viewData = $this->loadViewData();

        $graph = $this->getGraph();
        // Get user's timezone
        $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);

        // Append query parameters to the '/me/calendarView' url
        $getEventsUrl = '/me/calendar/events';

        $events = $graph->createRequest('GET', $getEventsUrl)
            // Add the user's timezone to the Prefer header
            ->addHeaders(array(
                'Prefer' => 'outlook.timezone="' . $viewData['userTimeZone'] . '"'
            ))
            ->setReturnType(Model\Event::class)
            ->execute();

        // Store Events into database
        foreach($events as $event) {
            $eventData[] = [
                'microsoft_event_id' => $event->getId(),
                'subject' => $event->getSubject(),
                'organizer' => $event->getOrganizer()->getEmailAddress()->getName(),
                'start_date' => Carbon::parse($event->getStart()->getDateTime())->format('Y-m-d h:i:s'),
                'end_date' => Carbon::parse($event->getEnd()->getDateTime())->format('Y-m-d h:i:s'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }
        $allEvents = Event::insert($eventData);

        $viewData['events'] = $events;
        return view('calendar', $viewData);
    }

    private function getGraph(): Graph
    {
        // Get the access token from the cache
        $tokenCache = new TokenCache();
        $accessToken = $tokenCache->getAccessToken();

        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);
        return $graph;
    }
}
